--Gaussian blur filter
--
--      |1 2 1|
-- 1/16 |2 4 2|
--      |1 2 1|
--
-- the largest multiplication 225 * 4 = 1020 - 10-bit
-- (result is shifted jon normalise koef(1\16) in the last register)


library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity MULT is
    generic(
      N : integer
    );
    port ( 
        pixel    : in std_logic_vector (N-1 downto 0);
        factor   : in std_logic_vector (N-1 downto 0);
        mult_out : out std_logic_vector (2*N-1 downto 0) --:= (others => '0')
      );
end MULT;

architecture MULT_struct  of  MULT is

  --signal 

begin

  process(pixel, factor)
  begin
    mult_out <= std_logic_vector(unsigned(pixel) * TO_INTEGER(signed(factor)));
    -- mult_out <= std_logic_vector(unsigned(pixel) * signed(factor));
  end process;

end MULT_struct ; -- MULT_struct 