--Gaussian blur filter
--
--      |1 2 1|
-- 1/16 |2 4 2|
--      |1 2 1|
--
-- the largest multiplication 225 * 4 = 1020 - 10-bit
-- (result is shifted jon normalise koef(1\16) in the last register)

library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity MULT_test is
  generic(
    N : integer := 8       -- 8
  );
end MULT_test;
architecture MULT_test_struct of MULT_test is

  component MULT 
    generic(
      N : integer := N
    );
    port ( 
        pixel    : in std_logic_vector (N-1 downto 0);
        factor   : in std_logic_vector (N-1 downto 0);
        mult_out : out std_logic_vector (2*N-1 downto 0)
      );
  end component;

  signal pixel    : std_logic_vector (N-1 downto 0);
  signal factor   : std_logic_vector (N-1 downto 0);
  signal mult_out : std_logic_vector (2*N-1 downto 0) := (others => '0');

begin

  circ_mult: MULT 
    port map( 
        pixel    => pixel,    
        factor   => factor,    
        mult_out => mult_out 
      );



pixel <= X"00" after 20 ns,
         X"8C" after 40 ns,
         X"C7" after 60 ns,
         X"0A" after 80 ns,
         X"60" after 100 ns,
         X"80" after 120 ns,
         X"A0" after 140 ns;



factor <= X"01" after 20 ns,
          X"05" after 40 ns,
          X"07" after 60 ns,
          X"0A" after 80 ns,
          X"F0" after 100 ns,
          X"7F" after 120 ns,
          X"A0" after 140 ns;
end MULT_test_struct ; -- MULT_test_struct