library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity REGsh_test is
  generic(
    N : integer := 8        -- 16
  );
end REGsh_test;

architecture REG_test_struct of REGsh_test is

  component REGsh 
    generic(
      N : integer := N        -- 16
    );
    port (
    clk : in std_logic;
    shift   : in natural;
    inaData : in std_logic_vector(N-1 downto 0);
    outData : out std_logic_vector(N-1 downto 0)
    ) ;
  end component;

  signal clk     : std_logic := '0';
  signal shift   : natural := 1;
  signal inaData : std_logic_vector(N-1 downto 0);
  signal outData : std_logic_vector(N-1 downto 0);

begin


circ_regsh :  REGsh 
  port map(
  clk     => clk,
  shift   => shift,
  inaData => inaData,
  outData => outData
  ) ;

process
begin 
  wait for 20 ns;
  clk <= not clk;
end process;

inaData <= X"80" after 20 ns,
           X"A0" after 40 ns,
           X"40" after 60 ns,
           X"20" after 80 ns,
           X"c0" after 100 ns;




end REG_test_struct ; -- REG_test_struct