library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CONV_test is
end CONV_test;

architecture CONV_test_struct of CONV_test is

component CONV 
  port (
      clk    : in std_logic;
  --    enable : in std_logic;
      shift  : in natural;
  
      info_1 : in std_logic_vector(7 downto 0);
      info_2 : in std_logic_vector(7 downto 0);
      info_3 : in std_logic_vector(7 downto 0);
      info_4 : in std_logic_vector(7 downto 0);
      info_5 : in std_logic_vector(7 downto 0);
      info_6 : in std_logic_vector(7 downto 0);
      info_7 : in std_logic_vector(7 downto 0);
      info_8 : in std_logic_vector(7 downto 0);
      info_9 : in std_logic_vector(7 downto 0);
      cnvOUT : out std_logic_vector(15 downto 0)
  );
end component;


  signal  clk    : std_logic := '0';
  --signal  reset  : std_logic;
  signal  shift  : natural := 0;
  signal  info_1 : std_logic_vector(7 downto 0) := (others => '0');
  signal  info_2 : std_logic_vector(7 downto 0) := (others => '0');
  signal  info_3 : std_logic_vector(7 downto 0) := (others => '0');
  signal  info_4 : std_logic_vector(7 downto 0) := (others => '0');
  signal  info_5 : std_logic_vector(7 downto 0) := (others => '0');
  signal  info_6 : std_logic_vector(7 downto 0) := (others => '0');
  signal  info_7 : std_logic_vector(7 downto 0) := (others => '0');
  signal  info_8 : std_logic_vector(7 downto 0) := (others => '0');
  signal  info_9 : std_logic_vector(7 downto 0) := (others => '0');
  signal  cnvOUT :  std_logic_vector(15 downto 0) := (others => '0');


begin

process
begin
    wait for 20 ns;
    clk <= not clk;
end process;

info_1 <= "00000001" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
info_2 <= "00000010" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
info_3 <= "00000011" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
info_4 <= "00000100" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
info_5 <= "00000101" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
info_6 <= "00000110" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
info_7 <= "00000111" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
info_8 <= "00001000" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
info_9 <= "00001001" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns; 


circ_conv: CONV 
  port map(
      clk => clk  ,
      shift => shift ,
      info_1 => info_1 ,
      info_2 => info_2 ,
      info_3 => info_3 ,
      info_4 => info_4 ,
      info_5 => info_5 ,
      info_6 => info_6 ,
      info_7 => info_7 ,
      info_8 => info_8 ,
      info_9 => info_9 ,
      cnvOUT => cnvOUT 
);

end CONV_test_struct ; -- CONV_test_struct

