library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sum_test is
end sum_test;


architecture sum_test_struct of sum_test is
component  sum is
    port (
        Data_1 : in std_logic_vector(15 downto 0);
        Data_2 : in std_logic_vector(15 downto 0);
        Data_3 : in std_logic_vector(15 downto 0);
        Data_4 : in std_logic_vector(15 downto 0);
        Data_5 : in std_logic_vector(15 downto 0);
        Data_6 : in std_logic_vector(15 downto 0);
        Data_7 : in std_logic_vector(15 downto 0);
        Data_8 : in std_logic_vector(15 downto 0);
        Data_9 : in std_logic_vector(15 downto 0);
        Data_out : out std_logic_vector(15 downto 0)
    );
end component;

signal Data_1 : std_logic_vector(15 downto 0) := (others => '0');
signal Data_2 : std_logic_vector(15 downto 0) := (others => '0');
signal Data_3 : std_logic_vector(15 downto 0) := (others => '0');
signal Data_4 : std_logic_vector(15 downto 0) := (others => '0');
signal Data_5 : std_logic_vector(15 downto 0) := (others => '0');
signal Data_6 : std_logic_vector(15 downto 0) := (others => '0');
signal Data_7 : std_logic_vector(15 downto 0) := (others => '0');
signal Data_8 : std_logic_vector(15 downto 0) := (others => '0');
signal Data_9 : std_logic_vector(15 downto 0) := (others => '0');
signal Data_out : std_logic_vector(15 downto 0) := (others => '0');

begin

circ_sum:  sum 
    port map(
        Data_1 => Data_1,
        Data_2 => Data_2,
        Data_3 => Data_3,
        Data_4 => Data_4,
        Data_5 => Data_5,
        Data_6 => Data_6,
        Data_7 => Data_7,
        Data_8 => Data_8,
        Data_9 => Data_9,
        Data_out => Data_out
);

Data_1 <= "00000001" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
Data_2 <= "00000001" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
Data_3 <= "00000001" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
Data_4 <= "00000001" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
Data_5 <= "00000001" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
Data_6 <= "00000001" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
Data_7 <= "00000001" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
Data_8 <= "00000001" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  
Data_9 <= "00000001" after 20 ns, "00000010" after 40 ns, "11111000" after 60 ns, "00001101" after 80 ns;  

end sum_test_struct;