onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /core_gen_test/circ_cre_gen/clk
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/hc
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/vc
add wave -noupdate /core_gen_test/circ_cre_gen/vidon
add wave -noupdate /core_gen_test/circ_cre_gen/w_en
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_pg/r_chanel
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_pg/g_chanel
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_core_red/circ_cnv/info_1
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_core_red/circ_cnv/info_2
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_core_red/circ_cnv/info_3
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_core_red/circ_cnv/info_4
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_core_red/circ_cnv/info_5
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_core_red/circ_cnv/info_6
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_core_red/circ_cnv/info_7
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_core_red/circ_cnv/info_8
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_core_red/circ_cnv/info_9
add wave -noupdate /core_gen_test/circ_cre_gen/cnv_en
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_core_red/circ_cnv/cnvOUT
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/circ_core_red/coreOUT
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/vga_vs
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/vga_hs
add wave -noupdate -radix unsigned /core_gen_test/circ_cre_gen/vga_r
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1061860 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1024484 ns} {1099236 ns}
