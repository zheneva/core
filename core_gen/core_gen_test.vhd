library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity core_gen_test is

end core_gen_test;

architecture core_gen_test_struct of core_gen_test is

component core_gen 
        generic(
          --mode : boolean := true;
          H_pixel   : integer :=  640; -- //? 640; 
          V_lines   : integer :=  480;-- //? 480;
          H_f_porch : integer :=  16;  -- //? 16 ; 
          H_b_porch : integer :=  48;  -- //? 16 ; 
          H_s_pulse : integer :=  96;  -- //? 128;
          V_f_porch : integer :=  10; -- //? 10 ; 
          V_b_porch : integer :=  29; -- //? 29 ;
          V_s_pulse : integer :=  2  -- //? 2
          );
          port (
            clk : in std_logic;
            clr : in std_logic;
            vga_hs   : out std_logic;
            vga_vs   : out std_logic;
            vga_r : out std_logic_vector(4 downto 0)
            --vga_g : out std_logic_vector(5 downto 0);
            --vga_b : out std_logic_vector(4 downto 0)
          );
end component;

signal clk     : std_logic:='0'; 
signal clr     : std_logic:='0'; 
signal core_gen_out : std_logic_vector(4 downto 0);


signal vga_hs :  std_logic;
signal vga_vs :  std_logic;
signal vga_r  :  std_logic_vector(4 downto 0);

begin

process
 begin
    wait for 20 ns;
    clk <= not clk;
end process;


circ_cre_gen: core_gen 
    port map(
        clk => clk,
        clr => clr,
        vga_hs => vga_hs,
        vga_vs => vga_vs,
        vga_r => vga_r
);

end core_gen_test_struct ; -- core_gen_test_struct


