library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CORE_test is
end CORE_test;


architecture CORE_test_struct of CORE_test is

component CORE 
  port (
      clk  : in std_logic;
      w_en : in std_logic;
    inDATA : in std_logic_vector(7 downto 0);
     shift : in natural;
   coreOUT : out std_logic_vector(15 downto 0)
  );
end component;

signal clk     : std_logic:='0'; 
signal w_en    : std_logic:='0';
signal inDATA  : std_logic_vector(7 downto 0);
signal shift   : natural := 0;
signal coreOUT : std_logic_vector(15 downto 0);

begin

process
begin
    wait for 20 ns;
    clk <= not clk;
end process;

inDATA  <=  "00000000" after 20 ns,
            "00000000" after 60 ns,
            "00000001" after 100 ns,
            "00000010" after 140 ns,
            "00000011" after 180 ns,
            "00000100" after 220 ns,
            "00000101" after 260 ns,
            "00000110" after 300 ns,
            "00000111" after 340 ns,            
            "00001000" after 380 ns,
            "00001001" after 420 ns,
            "00001010" after 460 ns,            
            "00000000" after 500 ns,
            "00000000" after 540 ns,
            "00000000" after 580 ns,

            "00000000" after 620 ns,
            "00000000" after 660 ns,
            "00000000" after 700 ns,
            "00001011" after 740 ns,
            "00001100" after 780 ns,
            "00001101" after 820 ns,
            "00001110" after 860 ns,
            "00001111" after 900 ns,
            "00010000" after 940 ns,
            "00010001" after 980 ns,
            "00010010" after 1020 ns,
            "00010011" after 1060 ns,
            "00010100" after 1100 ns,
            "00000000" after 1140 ns,
            "00000000" after 1180 ns,
            "00000000" after 1220 ns,

            "00000000" after 1260 ns,
            "00000000" after 1300 ns,
            "00000000" after 1340 ns,
            "00010101" after 1380 ns,
            "00010110" after 1420 ns,
            "00010111" after 1460 ns,
            "00011000" after 1500 ns,
            "00011001" after 1540 ns,
            "00011010" after 1580 ns,
            "00011011" after 1620 ns,
            "00011100" after 1660 ns,
            "00011101" after 1700 ns,
            "00011110" after 1740 ns,
            "00000000" after 1780 ns,
            "00000000" after 1820 ns,
            "00000000" after 1860 ns,

            "00000000" after 1900 ns,
            "00000000" after 1940 ns,
            "00000000" after 1980 ns,
            "00011111" after 2020 ns,
            "00100000" after 2060 ns,
            "00100001" after 2100 ns,
            "00100010" after 2140 ns,
            "00100011" after 2180 ns,
            "00100100" after 2220 ns,
            "00100101" after 2260 ns,
            "00100110" after 2300 ns,
            "00100111" after 2340 ns,
            "00101000" after 2380 ns,
            "00101001" after 2420 ns,
            "00000000" after 2460 ns,
            "00000000" after 2500 ns,
            "00000000" after 2540 ns,
            "00000000" after 2580 ns,
            "00000000" after 2620 ns,
            "00101001" after 2660 ns,
            "00101010" after 2700 ns,
            "00101011" after 2740 ns,
            "00101100" after 2780 ns,
            "00101101" after 2820 ns,
            "00101110" after 2860 ns,
            "00101111" after 2900 ns,
            "00110000" after 2940 ns,
            "00110001" after 2980 ns,
            "00110010" after 3020 ns,
            "00110011" after 3060 ns,
            "00000000" after 3100 ns,
            "00000000" after 3140 ns,
            "00000000" after 3180 ns;



w_en <= '1' after 100 ns,   '0' after 500 ns,  '1' after 740 ns,
        '0' after 1140 ns,  '1' after 1380 ns,
        '0' after 1780 ns, '1' after 2020 ns,  
        '0' after 2420 ns, '1' after 2660 ns,  
        '0' after 3060 ns, '1' after 3300 ns,
        '0' after 3700 ns, '1' after 3940 ns,
        '0' after 4340 ns, '1' after 4580 ns,
        '0' after 4980 ns, '1' after 5220 ns;

-- r_en <= '0' after 460 ns,  '1' after 700 ns,
--         '0' after 1100 ns,  '1' after 1340 ns,
--         '0' after 1740 ns, '1' after 1980 ns,  
--         '0' after 2380 ns, '1' after 2620 ns,  
--         '0' after 3020 ns, '1' after 3260 ns,
--         '0' after 3660 ns, '1' after 3900 ns,
--         '0' after 4300 ns, '1' after 4540 ns,
--         '0' after 4940 ns, '1' after 5180 ns;

   
circ_core: CORE 
  port map(
       clk => clk,  
      w_en => w_en,  
    inDATA => inDATA,  
     shift => shift, 
   coreOUT => coreOUT
);

end CORE_test_struct ; -- CORE_test_struct

