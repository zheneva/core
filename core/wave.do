onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /core_test/circ_core/circ_buf/clk
add wave -noupdate /core_test/circ_core/circ_buf/circ_ram_1/w_en
add wave -noupdate -radix decimal /core_test/circ_core/circ_buf/inDATA
add wave -noupdate -radix decimal /core_test/circ_core/circ_buf/circ_ram_1/w_ADDR
add wave -noupdate -radix decimal -childformat {{/core_test/circ_core/circ_buf/circ_ram_1/ram(0) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_1/ram(1) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_1/ram(2) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_1/ram(3) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_1/ram(4) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_1/ram(5) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_1/ram(6) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_1/ram(7) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_1/ram(8) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_1/ram(9) -radix decimal}} -subitemconfig {/core_test/circ_core/circ_buf/circ_ram_1/ram(0) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_1/ram(1) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_1/ram(2) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_1/ram(3) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_1/ram(4) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_1/ram(5) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_1/ram(6) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_1/ram(7) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_1/ram(8) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_1/ram(9) {-height 15 -radix decimal}} /core_test/circ_core/circ_buf/circ_ram_1/ram
add wave -noupdate -radix decimal -childformat {{/core_test/circ_core/circ_buf/circ_ram_2/ram(0) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_2/ram(1) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_2/ram(2) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_2/ram(3) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_2/ram(4) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_2/ram(5) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_2/ram(6) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_2/ram(7) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_2/ram(8) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_2/ram(9) -radix decimal}} -subitemconfig {/core_test/circ_core/circ_buf/circ_ram_2/ram(0) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_2/ram(1) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_2/ram(2) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_2/ram(3) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_2/ram(4) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_2/ram(5) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_2/ram(6) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_2/ram(7) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_2/ram(8) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_2/ram(9) {-height 15 -radix decimal}} /core_test/circ_core/circ_buf/circ_ram_2/ram
add wave -noupdate -radix decimal -childformat {{/core_test/circ_core/circ_buf/circ_ram_3/ram(0) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_3/ram(1) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_3/ram(2) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_3/ram(3) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_3/ram(4) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_3/ram(5) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_3/ram(6) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_3/ram(7) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_3/ram(8) -radix decimal} {/core_test/circ_core/circ_buf/circ_ram_3/ram(9) -radix decimal}} -subitemconfig {/core_test/circ_core/circ_buf/circ_ram_3/ram(0) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_3/ram(1) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_3/ram(2) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_3/ram(3) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_3/ram(4) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_3/ram(5) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_3/ram(6) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_3/ram(7) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_3/ram(8) {-height 15 -radix decimal} /core_test/circ_core/circ_buf/circ_ram_3/ram(9) {-height 15 -radix decimal}} /core_test/circ_core/circ_buf/circ_ram_3/ram
add wave -noupdate -radix decimal /core_test/circ_core/circ_buf/Data_1
add wave -noupdate -radix decimal /core_test/circ_core/circ_buf/Data_2
add wave -noupdate -radix decimal /core_test/circ_core/circ_buf/Data_3
add wave -noupdate -radix decimal /core_test/circ_core/circ_buf/Data_4
add wave -noupdate -radix decimal /core_test/circ_core/circ_buf/Data_5
add wave -noupdate -radix decimal /core_test/circ_core/circ_buf/Data_6
add wave -noupdate -radix decimal /core_test/circ_core/circ_buf/Data_7
add wave -noupdate -radix decimal /core_test/circ_core/circ_buf/Data_8
add wave -noupdate -radix decimal /core_test/circ_core/circ_buf/Data_9
add wave -noupdate -radix decimal /core_test/circ_core/circ_cnv/info_1
add wave -noupdate -radix decimal /core_test/circ_core/circ_cnv/info_2
add wave -noupdate -radix decimal /core_test/circ_core/circ_cnv/info_3
add wave -noupdate -radix decimal /core_test/circ_core/circ_cnv/info_4
add wave -noupdate -radix decimal /core_test/circ_core/circ_cnv/info_5
add wave -noupdate -radix decimal /core_test/circ_core/circ_cnv/info_6
add wave -noupdate -radix decimal /core_test/circ_core/circ_cnv/info_7
add wave -noupdate -radix decimal /core_test/circ_core/circ_cnv/info_8
add wave -noupdate -radix decimal /core_test/circ_core/circ_cnv/info_9
add wave -noupdate -radix decimal /core_test/circ_core/circ_cnv/cnvOUT
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {725 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 313
configure wave -valuecolwidth 63
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {1454 ns}
