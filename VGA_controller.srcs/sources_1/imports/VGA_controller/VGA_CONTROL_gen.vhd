-- Signal 1680 x 1050 @ 60 Hz timing

-- Horizontal timing (line)                            Vertical timing (frame)

-- Scanline part   | Pixels   | Time [µs]               Frame part	  | Lines   | Time [ms]
-- Visible area	   | 1680	  | 11.417697431018         Visible area  | 1050	| 16.098953377735
-- Front porch	   | 104	  | 0.70680984096779        Front porch	  | 1	    | 0.015332336550224
-- Sync pulse	   | 184	  | 1.2505097186353         Sync pulse	  | 3	    | 0.045997009650673
-- Back porch	   | 288	  | 1.9573195596031         Back porch	  | 33	    | 0.5059671061574
-- Whole line	   | 2256	  | 15.332336550224         Whole frame	  | 1087	| 16.666249830094

----------------------------------------------------------------------------------------------

-- Signal 640 x 480 @ 60 Hz timing

-- Horizontal timing (line)                         Vertical timing (frame)

-- Scanline part   | Pixels   | Time [µs]               Frame part	  | Lines | Time [ms]
-- Visible area	   | 640	  | 11.417697431018         Visible area  | 480	  | 16.098953377735
-- Front porch	   | 16 	  | 0.70680984096779        Front porch	  | 10    | 0.015332336550224
-- Sync pulse	   | 128 	  | 1.2505097186353         Sync pulse	  | 2	  | 0.045997009650673
-- Back porch	   | 16 	  | 1.9573195596031         Back porch	  | 29	  | 0.5059671061574
-- Whole line	   | 800	  | 15.332336550224         Whole frame	  | 521   | 16.666249830094


---- This line demonstrates how to convert positive integers
----output_1a <= std_logic_vector(to_unsigned(input_1, output_1a'length));

library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all; --use ieee.std_logic_unsigned.all;

entity VGA_CONTROL_gen is 
generic(
        H_pixel   : integer; 
        V_lines   : integer;
        H_f_porch : integer; 
        H_b_porch : integer; 
        H_s_pulse : integer;
        V_f_porch : integer; 
        V_b_porch : integer;
        V_s_pulse : integer

  );
   port(
        clk, clr : in std_logic;
        vga_hs   : out std_logic;
        vga_vs   : out std_logic;
        vga_r    : out std_logic_vector(4 downto 0);
        vga_g    : out std_logic_vector(5 downto 0);
        vga_b    : out std_logic_vector(4 downto 0)
  );
end VGA_CONTROL_gen;

architecture VGA_CONTROL_gen_struct of VGA_CONTROL_gen is

component Pattern_gen 

  generic(
  mode : boolean := true;
  H_b_porch : integer := H_b_porch ;
  H_s_pulse : integer := H_s_pulse;
  H_area    : integer := H_pixel;
  


  V_s_pulse : integer := V_s_pulse;
  V_b_porch : integer := V_b_porch;
  V_area    : integer := V_lines 
  );
 
  port (
     --vidon : in std_logic;
     x_pos : in std_logic_vector(11 downto 0);               -- horizontal counter 
     y_pos : in std_logic_vector(11 downto 0);               -- vertical   counter
  r_chanel : out std_logic_vector(4 downto 0);
  g_chanel : out std_logic_vector(5 downto 0);
  b_chanel : out std_logic_vector(4 downto 0)
  ) ;
end component;

  signal hc, vc : std_logic_vector(11 downto 0) := (others => '0');                        -- counters

  signal vidon  : std_logic;                                          -- define when dispaly the data
  
  signal vc_enable : std_logic;                     -- counters
  signal clk_low   : std_logic := '0';

  signal r_chanel_pg : std_logic_vector(4 downto 0);
  signal g_chanel_pg : std_logic_vector(5 downto 0);
  signal b_chanel_pg : std_logic_vector(4 downto 0);

begin

-- test: try to devide freq

-- process(clk)
-- begin
--   if(rising_edge(clk)) then
--     if (clk_low = '0') then
--       clk_low <= '1';
--     else 
--       clk_low <= '0';
--     end if;
--   end if;
-- end process;


process(clk, clr)
  begin

  if (rising_edge(clk)) then 
    if (clr = '1') then
      hc <= (others => '0');
    else
      if hc = std_logic_vector(to_unsigned((H_f_porch + H_b_porch + H_s_pulse + H_pixel - 1), hc'length)) then   -- hc = 799
        hc <= (others => '0') ;
        vc_enable <= '1';
      else 
        hc <= hc + 1;
        vc_enable <= '0';
      end if;
    end if;
  end if;
end process;  

vga_hs <= '0' when hc < std_logic_vector(to_unsigned((H_s_pulse), vc'length)) else '1';  


process(clk, clr)
begin
  if (rising_edge(clk)) then 
    if (clr = '1') then 
      vc <= (others => '0');
    elsif (vc_enable = '1') then 
      if(vc = std_logic_vector(to_unsigned((V_f_porch + V_b_porch + V_s_pulse + V_lines - 1), vc'length))) then -- vc = 520
        vc <= (others => '0');
      else
        vc <= vc + 1;
      end if;
    else 
      vc <= vc;
    end if;
  end if;
end process;

vga_vs <= '1' when (vc < std_logic_vector(to_unsigned((V_s_pulse), vc'length))) else '0';  
--vga_vs <= '0' when (vc < std_logic_vector(to_unsigned((V_s_pulse), vc'length))) else '1';                              -- //! добавил по пикселю в конец лиапазонов
                                                                                                                       -- //! <= 784  <= 511
vidon <= '1' when (((hc > std_logic_vector(to_unsigned(( H_s_pulse + H_b_porch - 1), hc'length))) and  --  144 < hc <  784  --- проверить диапазоны, как будто не хвататет одного пикселя 784-144 = 640
                      (hc < std_logic_vector(to_unsigned(( H_s_pulse + H_b_porch + H_pixel), hc'length)))) and          --- но мы не включаем граничные значения поэтому фактиеческая длина окна - 784-144-1
                        ((vc < std_logic_vector(to_unsigned(( V_s_pulse + V_b_porch + V_lines), vc'length))) and -- 31 < vc < 511 
                           (vc > std_logic_vector(to_unsigned(( V_s_pulse + V_b_porch - 1), vc'length))))) else '0';


--process(hc, vidon)
--begin
--  vga_r <= (others => '0');
--  vga_g <= (others => '0');
--  vga_b <= (others => '0');

--  if (vidon = '1') then
--    vga_r <= (others => vc(3));
--    vga_g <= (others => not vc(3));
--  end if;
--end process;


circ_pg: Pattern_gen 
      port map(
        x_pos => hc,               -- horizontal counter 
        y_pos => vc,               -- vertical   counter
     r_chanel => r_chanel_pg,
     g_chanel => g_chanel_pg,
     b_chanel => b_chanel_pg 
      ) ;
      
      
process(vidon)
begin
  vga_r <= (others => '0');
  vga_g <= (others => '0');
  vga_b <= (others => '0');

  if (vidon = '1') then
    vga_r <= r_chanel_pg;
    vga_g <= g_chanel_pg;  
    vga_b <= b_chanel_pg;
  end if;
end process;


end VGA_CONTROL_gen_struct ; -- VGA_CONTROL_gen